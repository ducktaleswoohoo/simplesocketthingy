Note in case anyone else finds this:  
I made this to help a buddy at work with socket.io.  
There isn't much here, but if someone else stumbles upon it and it helps them, awesome.

So this is fairly simple.  I do not have a whole lot going on here.  The entire server side thing where communication goes main.js (client) -> app.js (main server) -> socket.js (socket server) -> main.js (client) might not be necessary, but I threw it in anyhow.  You probably only need main.js -> socket.js.

`git clone` > `npm i` > `npm start`

This will start both the main and socket servers.

Open http://localhost:8081 for the socket server (not necessary but you can see connections).  
Open http://localhost:8080 for the main server serving the client html/js.

The 3 files to pay attention to are:  
`./client/js/main.js`  
`./socket/io.js`  
`./app/io.js` <- not necessary if you don't need to emit to socket server from app server  
Look for notes in these files.

Note that I put this all in 1 repo for ease of use, but in splitting the Errors App, we'd want the socket server in its own repo so we could deploy it to Heroku separately.

Hope this helps.

Cheers :)