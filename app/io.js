// ignore this if 100% of your socket emits will be coming from the client

let ioClient = require('socket.io-client')

exports.init = app => {

    const socketUrl = 'http://localhost:8081'
    const socket = ioClient.connect(socketUrl, { query: `token=server`, 'forceNew' : true })

    app.post('/:type', (req, res) => { // this is just in here so that i can call this service from the client and then have
                                       // this app emit to the socket server in case you needed the server to emit info
                                       // to the socket outside of user interaction on client end

        let obj = {
            token: req.body.token,
            isFromServer: true // i just use this on the frontend to differentiate where the socket emit came from
        }

        if(req.body.message) {
            obj.message = req.body.message
        }

        socket.emit(req.params.type, obj) // emit type and object over to socket server
        res.json({emitted: true}) // just to receive a response on client for post call
    })

}