let main = (() => {

    const socketUrl = 'http://localhost:8081' // socket server url
    const myToken = Math.round(Math.random() * 100000)  // could be internal user id, jwt, whatever
    const socket = io.connect(socketUrl, { query: `token=+${myToken}`, 'forceNew' : true }) // connect to the socket server

    function addNewMessage(obj) { // adds message to #incoming div

        let from = obj.token == myToken ? 'me' : obj.token

        if(obj.isFromServer) {
            from += ' (via server)'
        }

        from = String(from).replace('+', '')

        document.getElementById('incoming').innerHTML += `
            <div class='from'>
                ${from}:
            </div><div class='message'>
                ${obj.message}
            </div>
        `
    }

    function changeIncomingColor(obj) { // changes background color of #incoming div
        document.getElementById('incoming').style.backgroundColor = obj.color
        obj.message = 'changed the color'
        addNewMessage(obj)
    }

    function updateDisplayedUsers(users) { // update list of all connected users

        let html = `<div class="title">Connected users:</div>`

        users.sort()
        users.forEach(u => {
            html += `<div class="user">${String(u).replace('+', '')}</div>`
        })

        document.getElementById('users').innerHTML = html
    }
    
    socket.on('message', obj => { // receives the emit from the socket server line 33 of ./socket/io.js => io.sockets.emit(type, obj) in this case a message type
        addNewMessage(obj)
    })

    socket.on('color', obj => { // receives the emit from the socket server line 33 of ./socket/io.js => io.sockets.emit(type, obj) in this case a color type
        changeIncomingColor(obj)
    })

    socket.on('users', users => { // receives the emit from the socket server line 55, 56, and 67 of ./socket/io.js
        updateDisplayedUsers(users)
    })

    return {

        loaded: () => {
            document.getElementById('me').innerHTML = `I am ${myToken}`
        },

        submitMessage: toSocket => { // handle button clicks in client

            const ta = document.getElementById('myText')
            const obj = { token: myToken, message: myText.value }

            if(toSocket) {
                socket.emit('message', obj) // emit directly to socket server
            }else{

                fetch('/message', { // call service on own server to initiate a socket call from server (main) to server (socket)
                        method: 'post',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify(obj)
                    })
                    .then(response => {
                        // console.log(response)
                    }).catch(err => {
                        console.log('submit failed', err)
                    })
            }
        },

        submitColor: toSocket => { // handle button clicks in client

            const obj = { token: myToken }

            if(toSocket) {
                socket.emit('color', obj) // emit directly to socket server
            }else{

                fetch('/color', { // call service on own server to initiate a socket call from server (main) to server (socket)
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(obj)
                })
                    .then(response => {
                        // console.log(response)
                    }).catch(err => {
                        console.log('submit failed', err)
                    })
            }
        }
    }

})()