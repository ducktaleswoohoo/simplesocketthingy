// socket server

const express = require('express')
const bodyParser = require('body-parser')
const logger = require('morgan')

const app = express()
const port = 8081

let users = [] // to keep track of connected users

const server = app.listen(port, () => {

    app.use(bodyParser.json({ limit: '50mb' }))
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(logger('dev'))

    require('./socket/io').init(server, users)  // this is where all of the socket server stuff is

    app.get('*', (req, res) => {
        return res.json({socketServerSays: 'Hi!', users}) // obviously not necessary, but i just threw this in
    })

    console.log(`Socket server running on http://localhost:${port}.`)
})