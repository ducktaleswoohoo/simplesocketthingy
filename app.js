// app

const express = require('express')
const bodyParser = require('body-parser')
const logger = require('morgan')
const path = require('path')

const app = express()
const port = 8080
const publicPath = path.normalize(__dirname + '/client')

const server = app.listen(port, () => {
    
    console.log(`Server running - ${port}.`)
    
    app.use(bodyParser.json({ limit: '50mb' }))
    app.use(bodyParser.urlencoded({ extended: true }))
    app.use(express.static(publicPath))
    app.use(logger('dev'))

    app.get('*', (req, res) => {
        return res.sendFile(path.resolve('./client/index.html'))
    })

    require('./app/io').init(app) // you dont need this if 100% of your socket emit calls are from the clients
})