let io  // eventually socket.io
let curColor = 'white'
const colors = [
    'lightblue',
    'lightcyan',
    'lightcoral',
    'lightgreen',
    'lightgray',
    'white'
]

function getRandomColor() {

    let arr = colors.slice(0)
    const idx = arr.findIndex(c => c == curColor)
    let rand

    arr.splice(idx, 1)
    rand = Math.floor(Math.random() * arr.length)
    curColor = arr[rand]

    return curColor
}

function emitType(type, obj) {

    // this is just specific to what i was doing to handle the only 2 emit types i have

    if(type == 'color') {
        obj.color = getRandomColor()
    }

    io.sockets.emit(type, obj) // emit back to the clients - io.sockets.emit will emit to all connected and i believe socket.emit emits back to the 1 client the socket server received an emit from - see line 55
}

exports.init = (server, users) => {

    io = require('socket.io')(server)

    io.on('connection', socket => {

        let token = socket.handshake.query.token // i am pretty sure the built in socket.handshake.t property is a unique 'token' but i put this in
                                                 /// cause Dan has something similar in MetroCare, plus it shows how you can add your own token

        users.push(token) // just to keep track of connected users - not sure if socket.io has it's own way of tracking this

        socket.on('message', obj => { // when a connected socket (typically the client, but also the other server in this demo) emits a message, it's received here
            emitType('message', obj)
        })

        socket.on('color', obj => { // when a connected socket emits a color (change request), it's received here
            emitType('color', obj)  // the only thing coming in here is who emitted as opposed to the above which also contains a message
        })

        socket.emit('users', users) // this only emits back to the user who just connected
        io.sockets.emit('users', users) // the above does not fulfill all needs, in this case, but i left it as an example
                                        // this notifies all parties of a new member

        socket.on('disconnect', () => { // for this demo, this is just used to update the users array to keep track of connections
    
            let idx = users.indexOf(socket.handshake.query.token)

            if(idx != -1) {
                users.splice(idx, 1)
            }
            
            io.sockets.emit('users', users) // notify all remaining parties that someone has departed
        })

        console.log(`>>>>>> ${token} connected to the socket server`)
    })
}